﻿using System.Collections.Generic;

namespace ItunesMuzScrapper.Service.Model.Itunes
{
    public class ItunesResult
    { 
        public int ResultCount { set; get; } 
         
        public List<ItunesMusic> Results { get; set; }
    }
}