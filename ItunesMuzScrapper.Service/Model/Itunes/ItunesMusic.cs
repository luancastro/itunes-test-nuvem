﻿namespace ItunesMuzScrapper.Service.Model.Itunes
{
    public class ItunesMusic
    {
        
        public string ArtistName { set; get; }

        public string TrackName { set; get; }

        public string ArtworkUrl100 { set; get; }

        public int TrackTimeMillis { set; get; }
    }
}