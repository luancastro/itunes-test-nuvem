﻿using Newtonsoft.Json;

namespace ItunesMuzScrapper.Service.Model.Muz
{
    public class MuzMusic
    {
        public string Artista { set; get; }

        [JsonProperty("musica")]
        public string Item { set; get; }

        public string ImagemAlbumUrl { set; get; }

        public int Duracao { set; get; }
    }
}