﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using ItunesMuzScrapper.Service.Model.Itunes;
using Newtonsoft.Json;

namespace ItunesMuzScrapper.Service.Model
{
    public class ItunesService
    {
        public async Task<ItunesResult> Search(string query)
        {
            HttpClient client = new HttpClient();
            Uri uri = new Uri($"https://itunes.apple.com/search?media=music&term={query}");

            try
            {
                var json = await client.GetStringAsync(uri);

                var result = JsonConvert.DeserializeObject<ItunesResult>(json);

                return result;
            }
            catch (Exception e) { return null; }
        }
    }
}