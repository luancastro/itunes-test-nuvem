﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ItunesMuzScrapper.Service.Model;
using ItunesMuzScrapper.Service.Model.Muz;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ITunesMuzScrapper.Api.Controllers
{
    [Route("api/[controller]")]
    public class MusicController : Controller
    {

        private readonly ItunesService _itunesService;
        public MusicController(ItunesService itunesService)
        {
            _itunesService = itunesService;
        }
        // GET api/values/5
        [HttpGet]
        public async Task<IActionResult> Get(string q)
        {
            if (String.IsNullOrWhiteSpace(q)) { return BadRequest(); }

            var result = await _itunesService.Search(q);

            if (result.ResultCount <= 0) { return NotFound(); }

            var musics = result.Results.Select(item => new MuzMusic
            {
                Artista = item.ArtistName,
                Duracao = item.TrackTimeMillis,
                ImagemAlbumUrl = item.ArtworkUrl100,
                Item = item.TrackName
            });


            return Ok(musics.ToList());

        }
    }
}